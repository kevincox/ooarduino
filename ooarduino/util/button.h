// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef BUTTON_H
#define BUTTON_H

#include <stdint.h>

#include <ooarduino/pin.h>
#include <ooarduino/util/longclock.h>

namespace OOArduino
{

class Button
{
	Pin input;
	bool last;

	LongClock *clock;
	unsigned long next;
	uint8_t debounce;

	uint8_t presses;

public:
	Button(Pin input, LongClock *clock, uint8_t debounce = 0);

	void setDebounce ( uint8_t ms );

	void update ( void );
	uint8_t getPresses ( void );
	void setPresses ( uint8_t p);
};

}

#endif // BUTTON_H
