// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "clock.h"

namespace OOArduino
{

void Clock::populateFrequencies()
{
	ticksperms = frequency /1000;
	ticksperus = ticksperms/1000;
}

Clock::Clock(TimerCounter t):
    timer(t)
{
	timer.setWaveformGenerationMode(TimerCounter::normal);
	timer.setCompareOutputMode(TimerCounter::A, TimerCounter::none);
	timer.setCompareOutputMode(TimerCounter::B, TimerCounter::none);

	setSource(timer.getSource());
}

void Clock::setSource ( TimerCounter::ClockSource s, long unsigned int extFreq )
{
	unsigned int ps;
	switch (s)
	{
	case TimerCounter::noClock:
		frequency = 0;
		break;
	case TimerCounter::p1:
		frequency = F_CPU;
		break;
	case TimerCounter::p8:
		frequency = F_CPU/8;
		break;
	case TimerCounter::p64:
		frequency = F_CPU/64;
		break;
	case TimerCounter::p256:
		frequency = F_CPU/256;
		break;
	case TimerCounter::p1024:
		frequency = F_CPU/1024;
		break;
	case TimerCounter::extFalling:
	case TimerCounter::extRising:
		frequency = extFreq;
		break;
	default:
		frequency = 0; // Turn off the clock.  Good for debugging.
	}

	populateFrequencies();
	timer.setSource(s);
}

void Clock::set(uint8_t t)
{
	return timer.set(t);
}

void Clock::setus(unsigned int t)
{
	this->set(t*ticksperus);
}
void Clock::setms(unsigned int t)
{
	this->set(t*ticksperms);
}
void Clock::sets(unsigned int t)
{
	this->set(t*frequency);
}

uint8_t Clock::get()
{
	return timer.get();
}

unsigned int Clock::getus()
{
	return this->get()/ticksperus;
}

unsigned int Clock::getms()
{
	return this->get()/ticksperms;
}

unsigned int Clock::gets()
{
	return this->get()/frequency;
}

}
