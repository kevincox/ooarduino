// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include <util/atomic.h>

#include "longclock.h"

namespace OOArduino
{

void LongClock::updateOverflow(LongClock *c)
{
	c->overflow++;
}

LongClock::LongClock(TimerCounter t):
    Clock(t),

    overflow(0)
{
	timer.enableOverflowISR(false);
	timer.setOverflowISRData(this);
	timer.setOverflowISR((InterruptCallback)&updateOverflow);
	timer.enableOverflowISR(true);
}

void LongClock::set(unsigned long t)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		overflow = t >> 8;
	}
	timer.set(t);
}

void LongClock::setus(unsigned long t)
{
	this->set(t*ticksperus);
}
void LongClock::setms(unsigned long t)
{
	this->set(t*ticksperms);
}
void LongClock::sets(unsigned int t)
{
	this->set(t*frequency);
}

unsigned long LongClock::get()
{
	unsigned long overflow;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		overflow = this->overflow;
	}

	return (overflow<<8) | timer.get();
}

unsigned long LongClock::getus()
{
	return this->get()/ticksperus;
}

unsigned long LongClock::getms()
{
	return this->get()/ticksperms;
}

unsigned int LongClock::gets()
{
	return this->get()/frequency;
}

}
