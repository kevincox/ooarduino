// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef CLOCK16_H
#define CLOCK16_H

#include <stdint.h>

#include <ooarduino/timercounter16.h>

namespace OOArduino
{

/// A 16-bit Clock for keeping time.
/**
  See the documentation for Clock.
*/
class Clock16
{
protected:
	TimerCounter16 timer;
	long unsigned int frequency;
	long unsigned int tickspers;
	long unsigned int ticksperms;
	long unsigned int ticksperus;

	void populateFrequencies ( void );

public:
	/// Constructor.
	/**
	  Takes a TimerCounter16 which it uses to get the times.  Setting or resetting
	  the TimerCounter16 passed in will affect this class.  Also, this class
	  will affect the TimerCounter passed in.

	  \param t The timer source.
	*/
	Clock16(TimerCounter16 t);

	/// Set the clock source to use.
	/**
	  If you are using an external clock source you must call this using the
	  second argument so that the frequency is known.

	  \param s The source to use.
	  \param extFreq If the timer source is one that relies on an external
	           source the frequency must be passed in here.
	*/
	virtual void setSource ( TimerCounter16::ClockSource s, long unsigned int extFreq = 0);

	/// Set the current time.
	/**
	  \param t The desired value in ticks.
	*/
	void set ( uint16_t t );
	/// Set the current time in microseconds.
	/**
	  \param t The desired value in microseconds.
	*/
	void setus ( unsigned long int t );
	/// Set the current time in milliseconds.
	/**
	  \param t The desired value in milliseconds.
	*/
	void setms ( unsigned long int t );
	/// Set the current time in seconds.
	/**
	  \param t The desired value in seconds.
	*/
	void sets ( unsigned long int t );

	/// Get the time in ticks.
	/**
	  \return The time in ticks.
	*/
	uint16_t get ( void );
	/// Get the time in microseconds.
	/**
	  \return The time in microseconds.
	*/
	unsigned long int getus ( void );
	/// Get the time in milliseconds.
	/**
	  \return The time in milliseconds.
	*/
	unsigned long int getms ( void );
	/// Get the time in seconds.
	/**
	  \return The time in seconds.
	*/
	unsigned long int gets ( void );
};

}

#endif // CLOCK16_H
