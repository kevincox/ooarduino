// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <stdlib.h>
#include <stdint.h>

namespace OOArduino
{

/// A Circular buffer.
/**
  This buffer holds items up to it's size in a FIFO fashion.  If added items
  overflow the capacity of the buffer the oldest items will be purged to make
  room for the new items.
*/
class RingBuffer
{
	size_t size; ///< The size of the buffer in bytes.

	uint8_t *buffer; ///< The first byte of the buffer.
	uint8_t *end;    ///< The last byte of the buffer.

	uint8_t *read;  ///< The next byte to read.
	uint8_t *write; ///< The place to write the next byte.

	/// Wrap the pointer into the buffer.
	/**
	  This will wrap a pointer that is past the end of the buffer back into
	  the appropriate point in the buffer.

	  \param p The pointer.
	  \return The new pointer to an item in the buffer.
	*/
	uint8_t *wrap ( uint8_t *p );

public:
	/// Constructor
	/**
	  Creates a buffer with size for \a size bytes.
	*/
	RingBuffer(size_t size);

	/// Check if the buffer is empty.
	/**
	  \return \c true if the buffer is empty.
	*/
	bool empty ( void );
	/// Check if the buffer is full.
	/**
	  The buffer is considered full if adding a new character would overwrite
	  the character that was next to be read.

	  \return \c true if the buffer is full.
	*/
	bool full ( void );
	/// The number of items in the buffer.
	/**
	  \return The number of items in the buffer.
	*/
	size_t length ( void );

	/// Add an item to a buffer.
	/**
	  This will add a byte to the end of the buffer.  If the buffer is full the
	  first byte will be dropped.  Use RingBuffer.full() before adding an item
	  to prevent losing the first item.
	*/
	void push ( uint8_t b );

	/// Look at the first item.
	/**
	  This will return the value of the first byte without removing it.
	*/
	uint8_t peek ( void );
	/// Get an item from the buffer.
	/**
	  Removes the first item from the buffer and returns it.

	  \return The first item of the buffer.
	*/
	uint8_t pop ( void );
};

}

#endif // RINGBUFFER_H
