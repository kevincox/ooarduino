// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "bufferedserial.h"

#include <util/atomic.h>
#include <util/delay.h>

namespace OOArduino
{

void BufferedSerial::txReadyISR ( BufferedSerial *s )
{
	if (s->txbuf.empty())
	{
		s->usart->enableTxReadyISR(false); // Disable, we have nothing to send.
	}
	else
	{
		//_delay_ms(2);
		s->usart->put(s->txbuf.pop());
	}
}

void BufferedSerial::rxCompleteISR ( BufferedSerial *s )
{
	uint8_t b = s->usart->get();

	s->rxbuf.push(b);
}

int BufferedSerial::streamPut ( char c, FILE *s )
{
	BufferedSerial *bs = (BufferedSerial*)fdev_get_udata(s);

	bs->put(c);

	return 0;
}

int BufferedSerial::streamGet ( FILE *s )
{
	BufferedSerial *bs = (BufferedSerial*)fdev_get_udata(s);

	return bs->get();
}

BufferedSerial::BufferedSerial ( Usart *usart, size_t rxSize, size_t txSize ):
    usart(usart),
    rxbuf(rxSize), txbuf(txSize?txSize:rxSize),

    disallowtxoverflow(true)
{
	usart->enableRxCompleteISR(false);
	usart->setRxCompleteISR((InterruptCallback)&BufferedSerial::rxCompleteISR);
	usart->setRxCompleteISRData(this);
	usart->enableRxCompleteISR(true);

	usart->enableTxReadyISR(false);
	usart->setTxReadyISR((InterruptCallback)&BufferedSerial::txReadyISR);
	usart->setTxReadyISRData(this);
}

bool BufferedSerial::available ( void )
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		return !rxbuf.empty();
	}
}

void BufferedSerial::enableTxOverflow ( bool enable )
{
	disallowtxoverflow = !enable;
}

void BufferedSerial::put ( uint8_t d )
{
	if (disallowtxoverflow)
	{
		bool full = true;
		while (full)
		{
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
			{
				full = txbuf.full();
			}
		}
	}

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		txbuf.push(d);
	}

	usart->enableTxReadyISR(true);
}

uint8_t BufferedSerial::get()
{
	while (!available())
		;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		return rxbuf.pop();
	}
}

uint8_t BufferedSerial::peek()
{
	while (!available())
		;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		return rxbuf.peek();
	}
}

void BufferedSerial::flush ( void )
{
	bool empty = false;
	while (!empty)
	{
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			empty = txbuf.empty();
		}
	}
}

FILE *BufferedSerial::getStream ( void )
{
	FILE *s = fdevopen(&BufferedSerial::streamPut, &BufferedSerial::streamGet);
	fdev_set_udata(s, this);
	return s;
}

}
