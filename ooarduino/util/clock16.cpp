// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "clock16.h"

namespace OOArduino
{

void Clock16::populateFrequencies()
{
	tickspers  = 1/frequency;
	ticksperms = tickspers /1000;
	ticksperus = ticksperms/1000;
}

Clock16::Clock16(TimerCounter16 t):
    timer(t)
{
	timer.setWaveformGenerationMode(TimerCounter16::normal);
	timer.setCompareOutputMode(TimerCounter16::A, TimerCounter16::none);
	timer.setCompareOutputMode(TimerCounter16::B, TimerCounter16::none);

	setSource(timer.getSource());
}

void Clock16::setSource ( TimerCounter16::ClockSource s, long unsigned int extFreq )
{
	unsigned int ps;
	switch (s)
	{
	case TimerCounter16::noClock:
		frequency = 0;
		break;
	case TimerCounter16::p1:
		frequency = F_CPU;
		break;
	case TimerCounter16::p8:
		frequency = F_CPU/8;
		break;
	case TimerCounter16::p64:
		frequency = F_CPU/64;
		break;
	case TimerCounter16::p256:
		frequency = F_CPU/256;
		break;
	case TimerCounter16::p1024:
		frequency = F_CPU/1024;
		break;
	case TimerCounter16::extFalling:
	case TimerCounter16::extRising:
		frequency = extFreq;
		break;
	default:
		frequency = 0; // Turn off the clock.  Good for debugging.
	}

	populateFrequencies();
	timer.setSource(s);
}

void Clock16::set(uint16_t t)
{
	return timer.set(t);
}

void Clock16::setus(unsigned long int t)
{
	this->set(t*ticksperus);
}
void Clock16::setms(unsigned long int t)
{
	this->set(t*ticksperms);
}
void Clock16::sets(unsigned long int t)
{
	this->set(t*tickspers);
}

uint16_t Clock16::get()
{
	return timer.get();
}

unsigned long int Clock16::getus()
{
	return this->get()/ticksperus;
}

unsigned long int Clock16::getms()
{
	return this->get()/ticksperms;
}

unsigned long int Clock16::gets()
{
	return this->get()/tickspers;
}

}
