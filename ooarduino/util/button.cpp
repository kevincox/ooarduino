// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "button.h"

namespace OOArduino
{

Button::Button ( Pin input, LongClock *clock, uint8_t debounce ):
    input(input),
    clock(clock),
    debounce(debounce)
{
	next = clock->getms();
	last = input.get();
}

void Button::setDebounce(uint8_t ms)
{
	next += debounce-ms; // Update the next time to the new debounce value.
	debounce = ms;
}

void Button::update ( void )
{
	if ( input.get() == last ) return;

	unsigned int current = clock->getms();

	if ( current < next ) return; // If enough time has passed.

	presses++;
	next = current+debounce;
}

uint8_t Button::getPresses ( void )
{
	return presses;
}

void Button::setPresses ( uint8_t p )
{
	presses = p;
}

}
