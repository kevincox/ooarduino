// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef LONGCLOCK_H
#define LONGCLOCK_H

#include <ooarduino/util/clock.h>

namespace OOArduino
{

/// Create a Clock for long timings.
/**
  This class works quite like Clock except it has an overflow counter that
  effectively extends the counter to a 40-bits.
*/
class LongClock : public Clock
{
protected:
	volatile unsigned long overflow;

	static void updateOverflow ( LongClock *c );

public:
	/// Constructor
	/**
	  Creates a LongClock.  The new LongClock will own the TimerCounter.
	*/
	LongClock(TimerCounter t);

	/// Set the current time.
	/**
	  \param t The desired value in ticks.
	*/
	void set ( unsigned long t );
	/// Set the current time in microseconds.
	/**
	  \param t The desired value in microseconds.
	*/
	void setus ( unsigned long t );
	/// Set the current time in milliseconds.
	/**
	  \param t The desired value in milliseconds.
	*/
	void setms ( unsigned long t );
	/// Set the current time in seconds.
	/**
	  \param t The desired value in seconds.
	*/
	void sets ( unsigned int t );

	/// Get the time in ticks.
	/**
	  \return The time in ticks.
	*/
	unsigned long get ( void );
	/// Get the time in microseconds.
	/**
	  \return The time in microseconds.
	*/
	unsigned long getus ( void );
	/// Get the time in milliseconds.
	/**
	  \return The time in milliseconds.
	*/
	unsigned long getms ( void );
	/// Get the time in seconds.
	/**
	  \return The time in seconds.
	*/
	unsigned int gets ( void );
};

}

#endif // LONGCLOCK_H
