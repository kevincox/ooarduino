// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef BUFFEREDSERIAL_H
#define BUFFEREDSERIAL_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <ooarduino/usart.h>
#include <ooarduino/util/ringbuffer.h>

namespace OOArduino
{

/// A simple interface for serial communication.
class BufferedSerial
{
	Usart *usart;

	RingBuffer rxbuf;
	RingBuffer txbuf;

	bool disallowtxoverflow;

	static void txReadyISR ( BufferedSerial *s );
	static void rxCompleteISR ( BufferedSerial *s );

	static int streamPut ( char c , FILE *s );
	static int streamGet ( FILE *s );

public:
	/// Constructor.
	/**
	  This creates a BufferedSerial.  The Usart that is passed in can be used
	  to control all of the settings such as baud rate.  The only things in
	  the Usart object that this class controls is the txReady ISR and the
	  rxComplete ISR.

	  \param usart The Usart to use for sending and receiving.
	  \param rxSize The size of the receive buffer.
	  \param txSize the size of the transmit buffer.  This defaults to the size
	           of \a rxBuffer.
	*/
	BufferedSerial(Usart *usart, size_t rxSize = 16, size_t txSize = 0);

	/// If data is available.
	/**
	  Checks if there is data ready to be read.

	  \return `true` if there is data waiting in the receive buffer.
	*/
	bool available ( void );

	/// Enable overflowing of the tx buffer.
	/**
	  By default the put function will block until there is room in the transmit
	  buffer for the new data.  Enabling transmit buffer overflow will result
	  in data overflow and lost data if data is written to the buffer faster
	  than it can be written out.

	  \param enable `true` to enable transmit buffer overflow or `false` to
	           disable.
	*/
	void enableTxOverflow ( bool enable );

	/// Send a byte over the serial.
	/**
	  \param d The byte to write.
	*/
	void put ( uint8_t d );

	/// Get the next byte.
	/**
	  Return the next byte, this function will block if no data is available.

	  \return The next byte.
	*/
	uint8_t get ( void );
	/// Show the next byte.
	/**
	  This shows the next byte without removing it.  This function will block
	  until the data is available.

	  \return The byte.
	*/
	uint8_t peek ( void );

	/// Flush the output buffer.
	/**
	  This will block until all of the data has been sent.
	*/
	void flush ( void );

	/// Get a file stream.
	/**
	  This will create and return a `FILE*` object for use with the functions in
	  `stdio.h`.  The `FILE*` can only be used as long as the object that
	  returned it is valid.

	  \return the `FILE*` .
	*/
	FILE *getStream ( void );
};

}

#endif // BUFFEREDSERIAL_H
