// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "ringbuffer.h"

#include <stdlib.h>

namespace OOArduino
{

RingBuffer::RingBuffer ( size_t size ):
	size(++size)
{
	buffer = (uint8_t*)malloc((size)*sizeof(uint8_t));
	if ( buffer == NULL ) size = 0;

	read = write = buffer;

	end = buffer+size-1;
}


uint8_t *RingBuffer::wrap ( uint8_t *p )
{
	while ( p > end ) p -= size;

	return p;
}

bool RingBuffer::empty ( void )
{
	return read == write;
}

bool RingBuffer::full ( void )
{
	uint8_t *next = write + 1;
	next = wrap(next);

	if ( next == read )
	{
		return true;
	}
	return false;
}

size_t RingBuffer::length ( void )
{
	if ( write >= read )
	{
		return write-read;
	}
	else
	{
		return (end-buffer) - (read-write);
	}
}

void RingBuffer::push ( uint8_t b )
{
	*write++ = b;

	write = wrap(write);

	if ( write == read ) // We are full.
	{
		read++; // Drop a character.
		read = wrap(read);
	}
}

uint8_t RingBuffer::peek ( void )
{
	if (empty()) return 0xFF; // Invalid character or -1 signed.

	return *read;
}

uint8_t RingBuffer::pop ( void )
{
	if (empty()) return 0xFF; // Invalid character or -1 signed.

	uint8_t b = *read++;
	read = wrap(read);
	return b;
}

}
