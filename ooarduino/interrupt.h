// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef INTERRUPT_H
#define INTERRUPT_H

namespace OOArduino
{
	typedef void(*InterruptCallback)(void*);
}

#ifdef INTERRUPT_CPP // The source file.
	#include <stdlib.h>
	#include <avr/interrupt.h>

	#define OOA_GIH(n) namespace OOArduino \
	                   { \
	                       InterruptCallback Interrupt##n = NULL; \
	                       void *InterruptData##n = NULL; \
	                   }; \
	                   ISR(n) \
	                   { \
	                       (*OOArduino::Interrupt##n)(OOArduino::InterruptData##n); \
	                   }
#else // Act as a header file.
	#define OOA_GIH(n) namespace OOArduino \
	                   { \
	                       extern InterruptCallback Interrupt##n; \
	                       extern void *InterruptData##n; \
	                   };
#endif

#ifdef USART_RX_vect
	#define USART0_RX_vect USART_RX_vect
	#define USART0_TX_vect USART_TX_vect
	#define USART0_UDRE_vect USART_UDRE_vect
#endif

OOA_GIH(TIMER0_COMPA_vect)
OOA_GIH(TIMER0_COMPB_vect)
OOA_GIH(TIMER0_OVF_vect)
OOA_GIH(TIMER1_CAPT_vect)
OOA_GIH(TIMER1_COMPA_vect)
OOA_GIH(TIMER1_COMPB_vect)
OOA_GIH(TIMER1_OVF_vect)
OOA_GIH(TIMER2_COMPA_vect)
OOA_GIH(TIMER2_COMPB_vect)
OOA_GIH(TIMER2_OVF_vect)

OOA_GIH(USART0_RX_vect)
OOA_GIH(USART0_TX_vect)
OOA_GIH(USART0_UDRE_vect)

#endif // INTERRUPT_H
