// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

/** \mainpage OOArduino

  \section intro_sec Introduction

  This library is designed to provide a simple interface to the AVR's hardware
  while maintaining a proper dependency-injected system.  This makes this
  library useful for large scale applications as well as small hobby projects.

  \section design Design Patterns

  This library has two levels of abstraction.  The lower level classes are found
  in the root directory and provide a direct interface to the hardware
  registers.  The higher level classes are located in sub directories and
  provide powerful high-level tools.  These classes rely on the lower level
  classes for hardware access.

  When passing objects to functions the general pattern is if it is being passed
  by value the class it was passed to becomes the owner of those registers and
  they should only be modified by functions in that class.  If you are passing a
  pointer to the class you still own it and you can change settings in that
  object to affect it's behavior.  Often a class will tell you what capabilities
  it requires to control for proper operation but leaves the other parameters
  to your control.

  \section website Project Home

  OOArduino's home is at it's bitbucket.  It has a wiki and bug-tracker as well
  as downloads and source code at https://bitbucket.org/kevincox/ooarduino.
 **/
