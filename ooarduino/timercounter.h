// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef TIMERCOUNTER_H
#define TIMERCOUNTER_H

#include <stdint.h>

#include <ooarduino/interrupt.h>

namespace OOArduino
{

/// TimerCounter Register Access
/**
  This is a front-end for Timer/Counter registers.  It is a low level class so
  it will likely no make sense unless you are familiar with how these registers
  work.  The documentation here does not cover what they do it merely provides
  a method of accessing and updating them.  For usage information look at the
  data-sheet of your processor.
*/
class TimerCounter
{
public:
	/// ID
	/**
	  The timer counter registers have some pairs of functionality such as
	  compare output and PWM.  These identifiers are used to select which one
	  you want to use.
	*/
	enum Id {
		A,
		B,
	};

	/// Clock Source
	enum ClockSource {
		noClock    = 0b000, ///< No clock, timer disabled.
		p1         = 0b001, ///< CPU clock, no prescaler.
		p8         = 0b010, ///< CPU clock 8x prescaler.
		p64        = 0b011, ///< CPU clock 64x prescaler.
		p256       = 0b100, ///< CPU clock 256x prescaler.
		p1024      = 0b101, ///< CPU clock 1024x prescaler.
		extRising  = 0b110, ///< External clock, trigger on rising edge.
		extFalling = 0b111  ///< External clock, trigger on falling edge.
	};

	/// Compare Output Mode
	/**
	  This determines the action to take when an output compare is successful.
	*/
	enum CompareOutputMode {
		none       = 0b00, ///< Take no action.

		toggleFlag = 0b01, ///< Toggle the output compare flag.
		reserved   = 0b01,

		clearFlag      = 0b10, ///< Clear the output compare flag.
		clearUpSetDown = 0b10, ///< Clear the output compare flag on rising, set on falling.

		setFlag    = 0b11,    ///< Set the output compare flag.
		setUpClearDown = 0b11 ///< Set the output compare flag on rising, clear on falling.

	};

	/// Waveform Generation Mode.
	enum WaveformGenerationMode {
		normal                = 0b000, ///< Normal, no generation.
		pwmPhaseCorrect       = 0b001, ///< Phase correct PWM.
		ctc                   = 0b010, ///< Clear timer on compare.
		fastPwm               = 0b011, ///< Fast PWM mode.
		reserved1             = 0b100,
		pwmPhaseCorrectSetTop = 0b101, ///< Phase correct PWM with top of OCRA.
		reserved2             = 0b110,
		fastPwmSetTop         = 0b111  ///< Fast PWM with top of OCRA.
	};

protected:
	volatile uint8_t *tccra; ///< TCCRnA
	volatile uint8_t *tccrb; ///< TCCRnB

	volatile uint8_t *tcnt; ///< TCNTn

	volatile uint8_t *ocra; ///< OCRnA
	volatile uint8_t *ocrb; ///< OCRnB

	volatile uint8_t *timsk; ///< TIMSKn
	volatile uint8_t *tifr;  ///< TIFRn

	InterruptCallback *overflow; ///< Timer overflow ISR.
	void **overflowData;         ///< Timer overflow ISR data.

	InterruptCallback *compareA; ///< Compare match A ISR.
	void **compareAData;         ///< Compare match A ISR data.
	InterruptCallback *compareB; ///< Compare match B ISR.
	void **compareBData;         ///< Compare match B ISR data.

public:
	/// Constructor
	/**
	  Creates a \c TimerCounter.  It is tedious to pass the arguments so you
	  can use the macro \c CREATE_TC to generate the register names.
	*/
	TimerCounter(volatile uint8_t *tccra, volatile uint8_t *tccrb,
	             volatile uint8_t *ocra, volatile uint8_t *ocrb,
	             volatile uint8_t *tcnt,
	             volatile uint8_t *timsk, volatile uint8_t *tifr,
	             InterruptCallback *overflow, void **overflowData,
	             InterruptCallback *compareA, void **compareAData,
	             InterruptCallback *compareB, void **compareBData);

	/// Set the clock source to use.
	void setSource ( ClockSource s );
	/// Get the clock source being used.
	ClockSource getSource ( void );

	/// Set the waveform generation mode to use.
	void setWaveformGenerationMode ( WaveformGenerationMode m );
	/// Get the waveform generation mode being used.
	WaveformGenerationMode getWaveformGenerationMode ( void );

	/// Set the compare output mode to use.
	/**
	  \param id The ID to set.
	  \param m The mode to set.
	*/
	void setCompareOutputMode ( Id id, CompareOutputMode m );
	/// Get the compare output mode being used.
	/**
	  \param The ID to get.
	  \returns The output mode being used.
	*/
	CompareOutputMode getCompareOutputMode ( Id id );

	/// Set the output compare value.
	/**
	  \param id The ID to set the value for.
	  \param v  The value to compare to.
	*/
	void setOutputCompare ( Id id, uint8_t v );
	/// Get the output compare value.
	/**
	  \param id The id to get.
	  \returns The output compare value.
	*/
	uint8_t getOutputCompare ( Id id );

	/// Enable the Compare Output ISR.
	/**
	  \param id The ID to allow the interrupt for.
	  \param bool \c true to enable and \c false to disable.
	*/
	void enableCompareISR  ( Id id, bool enable );
	/// Set the ISR for a compare match.
	/**
	  \param id The id to set the ISR for.
	  \param isr The callback function.
	*/
	void setCompareISR     ( Id id, InterruptCallback isr );
	/// Set the ISR data for a compare match.
	/**
	  \param id The id to set the data for.
	  \param data The data.
	*/
	void setCompareISRData ( Id id, void *data );

	/// Enable the Overflow ISR.
	/**
	  \param bool \c true to enable and \c false to disable.
	*/
	void enableOverflowISR ( bool enable );
	/// Set the ISR for an overflow.
	/**
	  \param isr The callback function.
	*/
	void setOverflowISR    ( InterruptCallback isr );
	/// Set the ISR data for an overflow.
	/**
	  \param data The data.
	*/
	void setOverflowISRData ( void *data );

	/// Get the current value of the timer.
	uint8_t get ( void );
	/// Set the value of the timer.
	void set ( uint8_t v );
};

}

#endif // TIMERCOUNTER_H
