// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "pins.h"

#include <avr/io.h>

#include <ooarduino/factory.h>

namespace OOArduino
{
	namespace Arduino
	{

		Pin getPin ( unsigned int id )
		{
			switch (id)
			{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				return CREATE_PIN(D, id);
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14: // Don't actually exist.
			case 15: //
				return CREATE_PIN(B, id-8);
			}
		}

		Pin getPinFromAnalog ( unsigned int id )
		{
			return CREATE_PIN(C, id);
		}

		TimerCounter::Id getPwmPin ( PwmController *c, unsigned int id )
		{
			switch (id)
			{
			case 3:
				*c = PwmController(CREATE_TC(2));
				return TimerCounter::B;
			case 5:
				*c = PwmController(CREATE_TC(0));
				return TimerCounter::B;
			case 6:
				*c = PwmController(CREATE_TC(0));
				return TimerCounter::A;
			case 9:
				//*c = PwmController(CREATE_TC(1));
				return TimerCounter::A;
			case 10:
				//*c = PwmController(CREATE_TC(1));
				return TimerCounter::B;
			case 11:
				*c = PwmController(CREATE_TC(2));
				return TimerCounter::A;
			default:
				*c = PwmController(TimerCounter(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL));
				return TimerCounter::A;
			}
		}
	}
}
