// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef ARDUINO_PINS_H
#define ARDUINO_PINS_H

#include <ooarduino/pin.h>
#include <ooarduino/pwmpin.h>

namespace OOArduino
{
	namespace Arduino
	{
		/// Get a digital IO pin.
		/**
		  This function returns a Pin object that corresponds to a digital IO pin
		  on the Arduino board.

		  \param id The ID of the digital IO pin.
		  \return The corresponding Pin.
		*/
		Pin getPin ( unsigned int id );
		/// Get an analog IO pin.
		/**
		  This function returns a Pin object that corresponds to an analog IO pin
		  on the Arduino board.

		  \param id The ID of the analog IO pin.
		  \return The corresponding Pin.
		*/
		Pin getPinFromAnalog ( unsigned int id );

		/// Get a PwmPin.
		/**
		  Get a pin corresponding to a PWM pin on the Arduino board.

		  \param c Where to store the PwmController.
		  \param id The ID of the PWM pin.  If the ID is not a valid PWM pin the
		           result is undefined.
		  \return Returns the ID that corresponds to the passed in ID.
		*/
		TimerCounter::Id getPwmPin ( PwmController &c, unsigned int id );
	}
}

#endif // PINS_H
