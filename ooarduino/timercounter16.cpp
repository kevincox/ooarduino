// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "timercounter16.h"

namespace OOArduino
{

TimerCounter16::TimerCounter16(volatile uint8_t *tccra, volatile uint8_t *tccrb,
                               volatile uint8_t *tccrc,
                               volatile uint16_t *ocra, volatile uint16_t *ocrb,
                               volatile uint16_t *tcnt):
	tccra(tccra),
    tccrb(tccrb),
    tccrc(tccrc),

	ocra(ocra),
	ocrb(ocrb),

	tcnt(tcnt)
{
}

void TimerCounter16::setSource ( TimerCounter16::ClockSource s )
{
	uint8_t ntccrb = *tccrb;
	*tccrb = ( ntccrb & ~0b00000111 ) | s;
}

TimerCounter16::ClockSource TimerCounter16::getSource()
{
	return (ClockSource)(*tccrb & 0b0000111);
}

void TimerCounter16::setWaveformGenerationMode(TimerCounter16::WaveformGenerationMode m)
{
	uint8_t wgm = m;

	///// Set the WGM 3:2 bits in TCCRxB.
	uint8_t t = *tccrb;
	*tccra = ( t & ~0b00011000 ) | (wgm>>2);

	wgm &= 0b00000011;

	///// Set the WGM 1:0 bits in TCCRxA.
	t = *tccra;
	*tccra = ( t & ~0b00000011 ) | wgm;
}

TimerCounter16::WaveformGenerationMode TimerCounter16::getWaveformGenerationMode()
{
	return (WaveformGenerationMode)(((*tccrb & 0b00001000) >> 1 ) | (*tccra & 0b00000011));
}

void TimerCounter16::setCompareOutputMode(TimerCounter16::Id id, TimerCounter16::CompareOutputMode m)
{
	uint8_t mask = m;

	unsigned int shift;
	switch (id)
	{
	case A:
		shift = 6;
		break;
	case B:
		shift = 4;
		break;
	}

	uint8_t t = *tccra;
	*tccra = (t & ~(0b00000011<<shift)) | (mask << shift);
}

TimerCounter16::CompareOutputMode TimerCounter16::getCompareOutputMode(TimerCounter16::Id id)
{
	uint8_t t = *tccra;
	switch (id)
	{
	case A:
		t >>= 6;
		break;
	case B:
		t >>= 4;
		t &= 0b00000011;
		break;
	}

	return (CompareOutputMode)t;
}

void TimerCounter16::setOutputCompare(TimerCounter16::Id id, uint16_t v)
{
	volatile uint16_t *ocr;

	switch (id)
	{
	case A:
		ocr = ocra;
		break;
	case B:
		ocr = ocrb;
		break;
	}

	*ocr = v;
}

uint16_t TimerCounter16::getOutputCompare(TimerCounter16::Id id)
{
	volatile uint16_t *ocr;

	switch (id)
	{
	case A:
		ocr = ocra;
		break;
	case B:
		ocr = ocrb;
		break;
	}

	return *ocr;
}

void TimerCounter16::enableNoiseCanceler ( bool enable )
{
	if (enable) *tccrb |=  0b10000000;
	else        *tccrb &= ~0b10000000;
}

uint16_t TimerCounter16::get( void )
{
	return *tcnt;
}

void TimerCounter16::set( uint16_t v )
{
	*tcnt = v;
}

}
