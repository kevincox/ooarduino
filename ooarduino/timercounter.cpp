// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "timercounter.h"

namespace OOArduino
{

TimerCounter::TimerCounter(volatile uint8_t *tccra, volatile uint8_t *tccrb,
                           volatile uint8_t *ocra, volatile uint8_t *ocrb,
                           volatile uint8_t *tcnt,
                           volatile uint8_t *timsk, volatile uint8_t *tifr,
                           InterruptCallback *overflow, void **overflowData,
                           InterruptCallback *compareA, void **compareAData,
                           InterruptCallback *compareB, void **compareBData):
    tccra(tccra),
    tccrb(tccrb),

    ocra(ocra),
    ocrb(ocrb),

    tcnt(tcnt),

    timsk(timsk),
    tifr(tifr),

    overflow(overflow),
    overflowData(overflowData),
    compareA(compareA),
    compareAData(compareAData),
    compareB(compareB),
    compareBData(compareBData)
{
}

void TimerCounter::setSource ( TimerCounter::ClockSource s )
{
	*tccrb = ( *tccrb & ~0b00000111 ) | s;
}

TimerCounter::ClockSource TimerCounter::getSource()
{
	return (ClockSource)(*tccrb & 0b0000111);
}

void TimerCounter::setWaveformGenerationMode(TimerCounter::WaveformGenerationMode m)
{
	uint8_t wgm = m;

	///// Set the WGM02 bit in TCCRxB.
	if ( wgm & 0b00000100 )
	{
		*tccrb |= 0b00001000;
		wgm &= 0b0000011;
	}
	else
	{
		*tccrb &= ~0b00001000;
	}

	///// Set the WGM01:0 bits in TCCRxA.
	*tccra = ( *tccra & ~0b00000011 ) | wgm;
}

TimerCounter::WaveformGenerationMode TimerCounter::getWaveformGenerationMode()
{
	return (WaveformGenerationMode)(((*tccrb & 0b00001000) >> 1 ) | (*tccra & 0b00000011));
}

void TimerCounter::setCompareOutputMode(TimerCounter::Id id, TimerCounter::CompareOutputMode m)
{
	uint8_t mask = m;

	unsigned int shift;
	switch (id)
	{
	case A:
		shift = 6;
		break;
	case B:
		shift = 4;
		break;
	}

	uint8_t t = *tccra;
	*tccra = (t & ~(0b00000011<<shift)) | (mask << shift);
}

TimerCounter::CompareOutputMode TimerCounter::getCompareOutputMode(TimerCounter::Id id)
{
	uint8_t t = *tccra;
	switch (id)
	{
	case A:
		t >>= 6;
		break;
	case B:
		t >>= 4;
		t &= 0b00000011;
		break;
	}

	return (CompareOutputMode)t;
}

void TimerCounter::setOutputCompare(TimerCounter::Id id, uint8_t v)
{
	volatile uint8_t *ocr;

	switch (id)
	{
	case A:
		ocr = ocra;
		break;
	case B:
		ocr = ocrb;
		break;
	}

	*ocr = v;
}

uint8_t TimerCounter::getOutputCompare(TimerCounter::Id id)
{
	volatile uint8_t *ocr;

	switch (id)
	{
	case A:
		ocr = ocra;
		break;
	case B:
		ocr = ocrb;
		break;
	}

	return *ocr;
}

void TimerCounter::enableCompareISR(TimerCounter::Id id, bool enable)
{
	uint8_t m;

	switch (id)
	{
	case A:
		m = 0b00000010;
		break;
	case B:
		m = 0b00000100;
		break;
	}

	if (enable) *timsk |=  m;
	else        *timsk &= ~m;
}

void TimerCounter::setCompareISR(TimerCounter::Id id, InterruptCallback isr)
{
	InterruptCallback *var;

	switch (id)
	{
	case A:
		var = compareA;
		break;
	case B:
		var = compareB;
		break;
	}

	*var = isr;
}

void TimerCounter::setCompareISRData(TimerCounter::Id id, void *data)
{
	void **d;

	switch (id)
	{
	case A:
		d = compareAData;
		break;
	case B:
		d = compareBData;
		break;
	}

	*d = data;
}

void TimerCounter::enableOverflowISR(bool enable)
{
	if (enable) *timsk |=  0b00000001;
	else        *timsk &= ~0b00000001;
}

void TimerCounter::setOverflowISR(InterruptCallback isr)
{
	*overflow = isr;
}

void TimerCounter::setOverflowISRData(void *data)
{
	*overflowData = data;
}

uint8_t TimerCounter::get( void )
{
	return *tcnt;
}

void TimerCounter::set( uint8_t v )
{
	*tcnt = v;
}

}
