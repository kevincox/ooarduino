// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include <avr/io.h>
#include <util/atomic.h>
#include <assert.h>

#include "pin.h"

using namespace OOArduino;

Pin::Pin(volatile uint8_t *ddr, volatile uint8_t *port, volatile uint8_t *pin,
         unsigned int bit, PinMode mode)
{
	assert( ddr  != NULL );
	assert( port != NULL );
	assert( pin  != NULL );
	assert( bit  <  8    );

	this->ddr = ddr;
	this->port = port;
	this->pin = pin;

	this->bit = 1 << bit;
	this->nbit = ~this->bit;

	setMode(mode);
}

void Pin::setMode( PinMode mode )
{
	this->mode = mode;
	switch (mode)
	{
	case in:
		*ddr  &= nbit; // Input.
		*port &= nbit; // Disable pull-up.
		break;
	case inPullUp:
		*ddr  &= nbit; // Input.
		*port |=  bit; // Enable pull-up.
		break;
	case out:
		*ddr |= bit; // Output.
		break;
	case keep:
		break;
	}
}

bool Pin::get ( void )
{
	return *pin & bit;
}

void Pin::set ( bool v )
{
	if ( mode != out ) return;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		if (v) *port |= bit;
		else   *port &= nbit;
	}
}

void Pin::toggle()
{
	if ( mode != out ) return;

	*pin = bit; // Should be atomic.
}
