// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef FACTORY_H
#define FACTORY_H

#ifndef PINFACTORY_H
#define PINFACTORY_H

#include <avr/io.h>

#include <ooarduino/pin.h>
#include <ooarduino/interrupt.h>
#include <ooarduino/timercounter.h>
#include <ooarduino/timercounter16.h>
#include <ooarduino/pwmcontroller.h>
#include <ooarduino/usart.h>

/// Create a Pin.
/**
  This is a shortcut for creating Pins.  It will generate the required register
  names for you.

  \param port The capital letter that denotes the port.
  \param bit The index of the bit that controls the pin.
  \return The Pin object.
*/
#define CREATE_PIN(port, ...) OOArduino::Pin(&DDR##port, &PORT##port, &PIN##port, __VA_ARGS__)

/// Create a TimerCounter.
/**
  This is a shortcut for creating TimerCounters.  It will generate the required
  register names for you.

  \param id The ID of the timer.
  \return The TimerCounter object.
*/
#define CREATE_TC(id) OOArduino::TimerCounter(&TCCR##id##A, &TCCR##id##B, \
                                              &OCR##id##A, &OCR##id##B, \
                                              &TCNT##id, \
                                              &TIMSK##id, &TIFR##id, \
                                              &OOArduino::InterruptTIMER##id##_OVF_vect,   &OOArduino::InterruptDataTIMER##id##_OVF_vect, \
                                              &OOArduino::InterruptTIMER##id##_COMPA_vect, &OOArduino::InterruptDataTIMER##id##_COMPA_vect, \
                                              &OOArduino::InterruptTIMER##id##_COMPB_vect, &OOArduino::InterruptDataTIMER##id##_COMPB_vect)

/// Create a TimerCounter16.
/**
  This is a shortcut for creating TimerCounter16s.  It will generate the required
  register names for you.

  \param id The ID of the timer.
  \return The TimerCounter16 object.
*/
#define CREATE_TC16(id) OOArduino::TimerCounter16(&TCCR##id##A, &TCCR##id##B, &TCCR##id##C, &OCR##id##A, &OCR##id##B, &TCNT##id)

/// Create a Usart.
/**
  This is a shortcut for creating Usarts.  It will generate the required register
  names for you.

  \param id The ID of the Usart.  If there is only one Usart on the chip use the
           ID of \c 0 .
  \return The Usart object.
*/
#define CREATE_USART(id) OOArduino::Usart(&UDR##id, \
                                          &UCSR##id##A, &UCSR##id##B, &UCSR##id##C, \
                                          &UBRR##id, \
                                          &OOArduino::InterruptUSART##id##_RX_vect, &OOArduino::InterruptDataUSART##id##_RX_vect, \
                                          &OOArduino::InterruptUSART##id##_TX_vect, &OOArduino::InterruptDataUSART##id##_TX_vect, \
                                          &OOArduino::InterruptUSART##id##_UDRE_vect, &OOArduino::InterruptDataUSART##id##_UDRE_vect)

#endif // PINFACTORY_H

#endif // FACTORY_H
