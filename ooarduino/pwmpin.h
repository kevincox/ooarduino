// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include <ooarduino/pwmcontroller.h>

#ifndef PWMPIN_H
#define PWMPIN_H

namespace OOArduino
{

/// Pwm Output Pin
/**
  A PwmPin comes in pairs and is owned by it's parent PwmController.
  The frequency of a PwmPin is controlled by it's controller therefore there is
  no way to change it provided only the pin.  This ensures that you can pass a
  PwmPin around without fear that it's twin will be affected.
*/
class PwmPin
{
	PwmController *controller; ///< The pin's parent.
	TimerCounter::Id id; ///< The pin's ID.

public:
	/// Constructor
	/**
	  Generally you will not call this, you will get a PwmPin from a controller.
	*/
	PwmPin(PwmController *controller, TimerCounter::Id id);

	/// Set the output load of the pin.
	void setLoad ( uint8_t load ) const;
};

}

#endif // PWMPIN_H
