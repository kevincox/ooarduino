// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef PWM_H
#define PWM_H

#include <stdint.h>

#include "timercounter.h"
namespace OOArduino { class PwmController; }
#include "pwmpin.h"

namespace OOArduino
{

/// A Pwm Controller
/**
  A PwmController controls two PwmPins.  A PwmController can control things that
  affect both PwmPins as well as the pins individually.
*/
class PwmController
{
protected:
	TimerCounter timer; ///< The registers we need.

public:
	/// The two PwmPins controlled by this PwmController.
	PwmPin a, b;

	/// Constructor.
	/**
	  While the constructor does not take the related output pin the pin will
	  not actually be set unless it is set as an output.
	*/
	PwmController(TimerCounter t);

	/// Set the clock source.
	void setSource(TimerCounter::ClockSource s);

	/// Set the duty cycle.
	/**
	  \param id The ID of the pin to affect.
	  \param load The duty cycle where \f$ 0 \ge load < 256 \f$.
	*/
	void setLoad(TimerCounter::Id id, uint8_t load);
};

}

#endif // PWM_H
