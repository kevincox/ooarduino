// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef TIMERCOUNTER16_H
#define TIMERCOUNTER16_H

#include <stdint.h>

namespace OOArduino
{

/// 16 Bit TimerCounter
/**
  An interface to 16 bit counters.  See the TimerCounter docs for usage
  information.
*/
class TimerCounter16
{
public:
	/// ID
	/**
	  The timer counter registers have some pairs of functionality such as
	  compare output and PWM.  These identifiers are used to select which one
	  you want to use.
	*/
	enum Id {
		A,
		B
	};

	/// Clock Source
	enum ClockSource {
		noClock    = 0b000, ///< No clock, timer disabled.
		p1         = 0b001, ///< CPU clock, no prescaler.
		p8         = 0b010, ///< CPU clock 8x prescaler.
		p64        = 0b011, ///< CPU clock 64x prescaler.
		p256       = 0b100, ///< CPU clock 256x prescaler.
		p1024      = 0b101, ///< CPU clock 1024x prescaler.
		extRising  = 0b110, ///< External clock, trigger on rising edge.
		extFalling = 0b111  ///< External clock, trigger on falling edge.
	};

	/// Compare Output Mode
	/**
	  This determines the action to take when an output compare is successful.
	*/
	enum CompareOutputMode {
		none       = 0b00, ///< Take no action.

		toggleFlag = 0b01, ///< Toggle the output compare flag.
		reserved1  = 0b01,

		clearFlag      = 0b10, ///< Clear the output compare flag.
		clearUpSetDown = 0b10, ///< Clear the output compare flag on rising, set on falling.

		setFlag    = 0b11,    ///< Set the output compare flag.
		setUpClearDown = 0b11 ///< Set the output compare flag on rising, clear on falling.

	};

	/// Waveform Generation Mode.
	enum WaveformGenerationMode {
		normal                 = 0b0000, ///< Normal, no output.
		pwmPhaseCorrect8       = 0b0001, ///< Phase correct PWM 8-bit.
		pwmPhaseCorrect9       = 0b0010, ///< Phase correct PWM 9-bit.
		pwmPhaseCorrect10      = 0b0011, ///< Phase correct PWM 10-bit.
		ctcOcr                 = 0b0100, ///< Clear timer on compare, with top of OCRA.
		fastPwm8               = 0b0101, ///< Fast PWM mode, 8-bit.
		fastPwm9               = 0b0110, ///< Fast PWM mode, 9-bit.
		fastPwm10              = 0b0111, ///< Fast PWM mode, 10-bit.
		pwmPhaseFreqCorrectIcr = 0b1000, ///< Phase and frequency correct PWM, top of ICR.
		pwmPhaseFreqCorrectOcr = 0b1001, ///< Phase and frequency correct PWM, top of OCRA.
		pwmPhaseCorrectIcr     = 0b1010, ///< Phase correct PWM, top of ICR.
		pwmPhaseCorrectOcr     = 0b1011, ///< Phase correct PWM, top of OCRA.
		ctcIcr                 = 0b1100, ///< Clear timer on compare, with top of ICR.
		reserved               = 0b1101,
		fastPwmIcr             = 0b1110, ///< Fast PWM with top of ICR.
		fastPwmOcr             = 0b1111  ///< Fast PWM with top of OCRA.
	};

protected:
	volatile uint8_t *tccra;
	volatile uint8_t *tccrb;
	volatile uint8_t *tccrc;

	volatile uint16_t *ocra;
	volatile uint16_t *ocrb;

	volatile uint16_t *tcnt;

public:
	/// Constructor
	/**
	  Creates a \c TimerCounter16.  It is tedious to pass the arguments so you
	  can use the macro \c CREATE_TC16 to generate the register names.
	*/
	TimerCounter16(volatile uint8_t *tccra, volatile uint8_t *tccrb, volatile uint8_t *tccrc,
	               volatile uint16_t *ocra, volatile uint16_t *ocrb,
	               volatile uint16_t *tcnt);

	/// Set the clock source to use.
	void setSource ( ClockSource s );
	/// Get the clock source being used.
	ClockSource getSource ( void );

	/// Set the waveform generation mode to use.
	void setWaveformGenerationMode ( WaveformGenerationMode m );
	/// Get the waveform generation mode being used.
	WaveformGenerationMode getWaveformGenerationMode ( void );

	/// Set the compare output mode to use.
	/**
	  \param id The ID to set.
	  \param m The mode to set.
	*/
	void setCompareOutputMode ( Id id, CompareOutputMode m );
	/// Get the compare output mode being used.
	/**
	  \param The ID to get.
	  \returns The output mode being used.
	*/
	CompareOutputMode getCompareOutputMode ( Id id );

	/// Set the output compare value.
	/**
	  \param id The ID to set the value for.
	  \param v  The value to compare to.
	*/
	void setOutputCompare ( Id id, uint16_t v );
	/// Get the output compare value.
	/**
	  \param id The id to get.
	  \returns The output compare value.
	*/
	uint16_t getOutputCompare( Id id );

	/// Enable the noise canceler.
	/**
	  \param enable \c true to enable \c false to disable.
	*/
	void enableNoiseCanceler ( bool enable );

	/// Get the current value of the timer.
	uint16_t get ( void );
	/// Set the value of the timer.
	void set ( uint16_t v );
};

}

#endif // TIMERCOUNTER16_H
