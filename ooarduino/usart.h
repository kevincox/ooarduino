// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef USART_H
#define USART_H

#include <stdint.h>

#include <ooarduino/interrupt.h>

namespace OOArduino
{
/// Usart Register Access
/**
  This is a front-end for the USART registers.  It is a low level class so
  it will likely no make sense unless you are familiar with how these registers
  work.  The documentation here does not cover what they do it merely provides
  a method of accessing and updating them.  For usage information look at the
  data-sheet of your processor.
*/
class Usart
{
public:
	/// The operation mode of the USART.
	enum UsartMode {
		async     = 0b00, ///< Asynchronous operation.
		sync      = 0b01, ///< Synchronous operation.
		reserved  = 0b10,
		masterSPI = 0b11  ///< Master SPI mode.
	};

	/// The parity mode.
	enum ParityMode {
		disabled  = 0b00, ///< Parity generation and checks disabled.
		reserved2 = 0b01,
		even      = 0b10, ///< Enabled, even parity.
		odd       = 0b11  ///< Enabled, odd parity.
	};

	/// Stop bit setting.
	enum StopBit {
		oneBit = 0,
		twoBit = 1
	};

	/// Character size.
	enum CharSize {
		x5Bit     = 0b000, ///< 5-bit.
		x6Bit     = 0b001, ///< 6-bit.
		x7Bit     = 0b010, ///< 7-bit.
		x8Bit     = 0b011, ///< 8-bit.
		reserved3 = 0b100,
		reserved4 = 0b101,
		reserved5 = 0b110,
		x9Bit     = 0b111  ///< 9-bit.
	};

protected:
	volatile uint8_t *udr;
	volatile uint8_t *uscra;
	volatile uint8_t *uscrb;
	volatile uint8_t *uscrc;
	volatile uint16_t *ubrr;

	InterruptCallback *rxCompleteISR;
	void **rxCompleteData;
	InterruptCallback *txCompleteISR;
	void **txCompleteData;
	InterruptCallback *txReadyISR;
	void **txReadyData;

public:
	/// Constructor
	/**
	  Creates a \c Usart.  It is tedious to pass the arguments so you can use
	  the macro \c CREATE_USART to generate the register names.
	*/
	Usart(volatile uint8_t *udr,
	      volatile uint8_t *uscra, volatile uint8_t *uscrb,
	      volatile uint8_t *uscrc,
	      volatile uint16_t *ubrr,
	      InterruptCallback *rxCompleteISR, void **rxCompleteData,
	      InterruptCallback *txCompleteISR, void **txCompleteData,
	      InterruptCallback *txReadyISR,    void **txReadyData);

	/// Data available.
	/**
	  If there is data available to be read.

	  \return \c true if there is data ready to be read.
	*/
	bool dataAvailable ( void );
	/// Transmitter ready.
	/**
	  If the transmitter is ready to receive another piece of data.

	  \return \c true if the transmitter is ready.
	*/
	bool txReady ( void );
	/// Transmission complete.
	/**
	  If there is no more data to be sent.

	  \return \c true if there is no more data to send.
	*/
	bool txComplete ( void );

	/// Frame error.
	/**
	  \return \c true if a frame error occurred.
	*/
	bool frameError ( void );
	/// Data overrun
	/**
	  \return \c true if a data overrun condition was detected.
	*/
	bool dataOverun ( void );
	/// Parity error.
	/**
	  \return \c true if the character had a parity error.
	*/
	bool parityError ( void );

	/// Enable double speed.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableDoubleSpeed ( bool enable );
	/// Double speed enabled.
	/**
	  \return \c true if double speed is enabled.
	*/
	bool doubleSpeedEnabled ( void );
	/// Enable multiprocessor mode.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableMultiprocessorMode ( bool enable );
	/// Multiprocessor mode enabled.
	/**
	  \return \c true if multiprocessor mode is enabled.
	*/
	bool multiprocessorModeEnabled ( void );

	/// Enable receive complete ISR.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableRxCompleteISR ( bool enable );
	/// Receive complete ISR enabled.
	/**
	  \return \c true if enabled.
	*/
	bool rxCompleteISREnabled ( void );
	/// Set receive complete ISR.
	/**
	  Set the receive receive ISR callback.

	  \param c The callback.
	*/
	void setRxCompleteISR ( InterruptCallback c );
	/// Set receive complete ISR data.
	/**
	  Set the receive receive ISR data.

	  \param d The data.
	*/
	void setRxCompleteISRData ( void *d );

	/// Enable transmit ready ISR.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableTxReadyISR ( bool enable );
	/// Transmit ready ISR enabled.
	/**
	  \return \c true if enabled.
	*/
	bool txReadyISREnabled ( void );
	/// Set transmit ready ISR.
	/**
	  Set the transmit ready ISR callback.

	  \param c The callback.
	*/
	void setTxReadyISR ( InterruptCallback c );
	/// Set transmit ready ISR data.
	/**
	  Set the transmit ready ISR data.

	  \param d The data.
	*/
	void setTxReadyISRData ( void *d );

	/// Enable transmit complete ISR.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableTxCompleteISR ( bool enable );
	/// Transmit complete ISR enabled.
	/**
	  \return \c true if enabled.
	*/
	bool txCompleteISREnabled ( void );
	/// Set transmit complete ISR.
	/**
	  Set the transmit receive ISR callback.

	  \param c The callback.
	*/
	void setTxCompleteISR ( InterruptCallback c );
	/// Set transmit complete ISR data.
	/**
	  Set the transmit receive ISR data.

	  \param d The data.
	*/
	void setTxCompleteISRData ( void *d );

	/// Enable receiver.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableReciever ( bool enable );
	/// Receiver enabled.
	/**
	  \return \c true if the receiver is enabled.
	*/
	bool recieverEnabled ( void );
	/// Enable transmitter.
	/**
	  \param enable \c true to enable, \c false to disable.
	*/
	void enableTransmitter ( bool enable );
	/// Transmitter enabled.
	/**
	  \return \c true if the transmitter is enabled.
	*/
	bool transmitterEnabled ( void );

	/// Set the parity mode.
	void setParityMode ( ParityMode m );
	/// Get the parity mode.
	ParityMode getParityMode ( void );

	/// Set the stop bit mode.
	void setStopBit ( StopBit b );
	/// Get the stop bit mode.
	StopBit getStopBit ( void );

	/// Set the character size.
	void setCharSize ( CharSize s );
	/// Get the character size.
	CharSize getCharSize ( void );

	/// Set the baud rate.
	/**
	  This will calculate and apply the given baud rate.  If the baud rate is
	  too small and double speed is enabled it will automatically be disabled and
	  the baud rate adjusted to fit in the register.  If you really want double
	  speed enabled you can re-enable it after.

	  \param baud The desired baud rate.
	*/
	void setBaud ( unsigned long baud );
	/// Get the current baud rate.
	unsigned long getBaud ( void );

	/// Write a value to the USART transmitter.
	void put ( uint8_t c );
	/// Read a value from the Receiver.
	uint8_t get ( void );
};

}

#endif // USART_H
