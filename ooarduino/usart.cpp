// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#include "usart.h"

namespace OOArduino
{

Usart::Usart(volatile uint8_t *udr,
             volatile uint8_t *uscra, volatile uint8_t *uscrb,
             volatile uint8_t *uscrc,
             volatile uint16_t *ubrr,
             InterruptCallback *rxCompleteISR, void **rxCompleteData,
             InterruptCallback *txCompleteISR, void **txCompleteData,
             InterruptCallback *txReadyISR,    void **txReadyData):
    udr(udr),
    uscra(uscra), uscrb(uscrb), uscrc(uscrc), ubrr(ubrr),

    rxCompleteISR(rxCompleteISR), rxCompleteData(rxCompleteData),
    txCompleteISR(txCompleteISR), txCompleteData(txCompleteData),
    txReadyISR(txReadyISR),       txReadyData(txReadyData)
{
}

bool Usart::dataAvailable ( void )
{
	return *uscra & 0b10000000;
}

bool Usart::txReady ( void )
{
	return *uscra & 0b00100000;
}

bool Usart::txComplete ( void )
{
	return *uscra & 0b01000000;
}

bool Usart::frameError ( void )
{
	return *uscra & 0b00010000;
}

bool Usart::dataOverun ( void )
{
	return *uscra & 0b00001000;
}

bool Usart::parityError ( void )
{
	return *uscra & 0b00000100;
}

void Usart::enableDoubleSpeed ( bool enable )
{
	if (enable) *uscra |=  0b00000010;
	else        *uscra &= ~0b00000010;
}

bool Usart::doubleSpeedEnabled()
{
	return *uscra & 0b00000010;
}

void Usart::enableMultiprocessorMode(bool enable)
{
	if (enable) *uscra |=  0b00000001;
	else        *uscra &= ~0b00000001;
}

void Usart::put ( uint8_t c )
{
	*udr = c;
}

uint8_t Usart::get ( void )
{
	return *udr;
}

void Usart::enableRxCompleteISR ( bool enable )
{
	if (enable) *uscrb |=  0b10000000;
	else        *uscrb &= ~0b10000000;
}

bool Usart::rxCompleteISREnabled()
{
	return *uscrb & 0b10000000;
}

void Usart::setRxCompleteISR ( InterruptCallback c )
{
	*rxCompleteISR = c;
}

void Usart::setRxCompleteISRData ( void *d )
{
	*rxCompleteData = d;
}

void Usart::enableTxReadyISR ( bool enable )
{
	if (enable) *uscrb |=  0b00100000;
	else        *uscrb &= ~0b00100000;
}

bool Usart::txReadyISREnabled ( void )
{
	return *uscrb & 0b00100000;
}

void Usart::setTxReadyISR ( InterruptCallback c )
{
	*txReadyISR = c;
}

void Usart::setTxReadyISRData ( void *d )
{
	*txReadyData = d;
}

void Usart::enableTxCompleteISR(bool enable)
{
	if (enable) *uscrb |=  0b01000000;
	else        *uscrb &= ~0b01000000;
}

bool Usart::txCompleteISREnabled ( void )
{
	return *uscrb & 0b01000000;
}

void Usart::setTxCompleteISR ( InterruptCallback c )
{
	*txCompleteISR = c;
}

void Usart::setTxCompleteISRData ( void *d )
{
	*txCompleteData = d;
}

void Usart::enableReciever ( bool enable )
{
	if (enable) *uscrb |=  0b00010000;
	else        *uscrb &= ~0b00010000;
}

bool Usart::recieverEnabled ( void )
{
	return *uscrb & 0b00010000;
}

void Usart::enableTransmitter ( bool enable )
{
	if (enable) *uscrb |=  0b00001000;
	else        *uscrb &= ~0b00001000;
}

bool Usart::transmitterEnabled ( void )
{
	return *uscrb & 0b00001000;
}

void Usart::setParityMode ( ParityMode m )
{
	*uscrc = ( *uscrc & ~0b00110000 ) & ( m << 4 );
}

Usart::ParityMode Usart::getParityMode ( void )
{
	return (ParityMode)((*uscrc&0b00110000)>>4);
}

void Usart::setStopBit ( StopBit b )
{
	if (b) *uscrc |=  0b00001000;
	else   *uscrc &= ~0b00001000;
}

Usart::StopBit Usart::getStopBit ( void )
{
	return (StopBit)((*uscrc&0b00001000)>>3);
}

void Usart::setCharSize ( CharSize s )
{
	*uscrb = ( *uscrb & ~0b00000100 ) | ( s & 0b00000100 );
	*uscrc = ( *uscrc & ~0b00000110 ) | (( s & 0b00000011 ) << 1 );
}

Usart::CharSize Usart::getCharSize ( void )
{
	return (CharSize)((*uscrb & 0b00000100) | ((*uscrc & 0b00000110)>>1));
}

void Usart::setBaud(unsigned long baud)
{
	uint16_t baud_value = F_CPU / 8UL / baud - 1;

	if (doubleSpeedEnabled())
	{
		if ( baud_value > 4095 )
		{
			baud_value /= 2;
			enableDoubleSpeed(false);
		}
	}
	else baud_value /= 2;

	*ubrr = baud_value;
}

unsigned long Usart::getBaud()
{
	if (doubleSpeedEnabled()) return F_CPU /  8UL / *ubrr;
	else                      return F_CPU / 16UL / *ubrr;
}

}
