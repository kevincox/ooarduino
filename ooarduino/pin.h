// Copyright 2012 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *                                                           *
*                                                                              *
*******************************************************************************/

#ifndef PIN_H
#define PIN_H

#include <stdlib.h>
#include <stdint.h>

namespace OOArduino
{
class Pin
{
public:

	/// The mode of the pin.
	enum PinMode {
		keep,    ///< Leave the mode of the pin the same.

		out,     ///< Set the pin as an output.

		in,      ///< Set the pin as an input.
		inPullUp ///< Set the pin as an input with it's pull-up enabled.
	};

private:

	PinMode mode;

	volatile uint8_t *ddr;  ///< Pin direction.
	volatile uint8_t *port; ///< Port control.
	volatile uint8_t *pin;  ///< Pin value.
	uint8_t bit;   ///< Mask.
	uint8_t nbit;  ///< Inverted mask.

public:
	/// Create a pin.
	/**
	 * Creates a pin that is bit \a bit in the port passed.  It is tedious to
	 * pass the arguments so you can use the macro \c CREATE_PIN to generate the
	 * port names.
	 */
	Pin(volatile uint8_t *ddr, volatile uint8_t *port, volatile uint8_t *pin,
	    unsigned int bit, PinMode mode = keep);

	/// Set the direction of the pin.
	/**
	 * \param mode The direction to set the pin to.
	 */
	void setMode ( PinMode mode );

	/// Get the state of the pin.
	/**
	 * \returns The state of the pin, \c true for high or \c false for low.
	 */
	bool get ( void );

	/// Set the state of the pin.
	/**
	 * \param v Set the state of the pin, \c true for high or \c false for low.
	            if the pin is not an output this does nothing.
	 */
	void set ( bool v );

	/// Toggle the state of the pin.
	void toggle ( void );
};

}

#endif // PIN_H
